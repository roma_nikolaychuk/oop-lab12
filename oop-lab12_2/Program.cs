﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace oop_lab12_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            List<Goods> goods = new List<Goods>();

            goods.Add(new Goods("Ноутбук", 22500, 1.6));
            goods.Add(new Goods("Телефон", 1800, 0.18));
            goods.Add(new Goods("Телефізор", 20799, 11.3));
            goods.Add(new Goods("Планшет", 7690.5, 0.35));

            foreach (Goods x in goods) x.Info();
            
            Console.ReadLine();
        }
    }
}
