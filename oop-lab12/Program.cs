﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using ClassLibrary;

namespace oop_lab12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            StreamReader fileIn = new StreamReader("dyzhki.txt");
            string line = fileIn.ReadToEnd();
            fileIn.Close();
            Stack duzhky = new Stack();
            bool flag = true;
            //Перевіряємо баланс дужок
            for (int i = 0; i < line.Length; i++)
            {
                //якщо поточний символ дужка, що відкривається, то поміщаємо її в стек
                if (line[i] == '(') duzhky.Push(i);
                else if (line[i] == ')') //Якщо черговий символ дужка, що закривається, то
                {
                    //якщо стек порожній, то для закриваючої дужки не вистачає парної відкриваючої
                    if (duzhky.Count == 0)
                    {
                        flag = false; Console.WriteLine("Можливо в позиції " + i + " зайва ) дужка");
                    }
                    else duzhky.Pop(); //інакше витягуємо парну дужку
                }
            }
            //якщо після перегляду рядка стек виявився порожнім, то дужки збалансовані
            if (duzhky.Count == 0)
            {
                if (flag) Console.WriteLine("дужки збалансовані");
            }
            else //інакше баланс дужок порушений
            {
                Console.Write("Можливо зайва ( дужка в позиції:");
                while (duzhky.Count != 0)
                {
                    Console.Write("{0} ", (int)duzhky.Pop());
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}