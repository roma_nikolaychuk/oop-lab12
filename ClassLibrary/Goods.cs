﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Goods
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private double price;

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        private double weight;

        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public Goods()
        {

        }

        public Goods(string name, double price, double weight)
        {
            this.name = name;
            this.price = price;
            this.weight = weight;
        }

        public Goods(Goods goods)
        {
            this.Name = goods.Name;
            this.Price = goods.Price;
            this.Weight = goods.Weight;
        }

        public void Info()
        {
            Console.WriteLine("Назва товару - {0}\nЦіна - {1}\nВага - {2}", Name, Price, Weight);
            Console.WriteLine();
        }
    }
}
